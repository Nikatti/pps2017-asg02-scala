package u04lab.code

trait Student {
  def name: String
  def year: Int
  def enrolling(course: Course): Unit // the student participates to a Course
  def courses: scala.List[String] // names of course the student participates to
  def hasTeacher(teacher: String): Boolean // is the student participating to a course of this teacher?
  def enrolling2(courses: Course*): Unit
}

trait Course {
  def name: String
  def teacher: String
}

case class CourseImpl(override val name: String,override val teacher: String) extends Course{}

case class StudentImpl(override val name: String,override val year: Int) extends Student{ // Che tipo di lista va usata la nostra o quella di scala?
  private var courseList: scala.List[Course] = scala.List()

  override def courses: scala.List[String] = courseList map (course => course.name)
  override def enrolling(course: Course): Unit = courseList ::= course
  override def hasTeacher(teacher: String): Boolean = (courseList  map (courses => courses.teacher)) contains teacher

  def enrolling2(courses: Course*): Unit = for(i <- courses.length-1 to 0 by -1) courseList ::= courses(i)
}

object Student {
  def apply(name: String, year: Int = 2017):Student = StudentImpl(name,year)
}

object Course {
  def apply(name: String, teacher: String):Course = CourseImpl(name,teacher)
}

object Try extends App {
  val cPPS = Course("PPS","Viroli")
  val cPCD = Course("PCD","Ricci")
  val cSDR = Course("SDR","D'Angelo")
  val s1 = Student("mario",2015)
  val s2 = Student("gino",2016)
  val s3 = Student("rino") //defaults to 2017
  s1.enrolling(cPPS)
  s1.enrolling(cPCD)
  s2.enrolling(cPPS)
  s3.enrolling(cPPS)
  s3.enrolling(cPCD)
  s3.enrolling(cSDR)
  println(s1.courses, s2.courses, s3.courses) // (Cons(PCD,Cons(PPS,Nil())),Cons(PPS,Nil()),Cons(SDR,Cons(PCD,Cons(PPS,Nil()))))
  println(s1.hasTeacher("Ricci")) // true
  val s4 = Student("nicola",2017)
  s4.enrolling2(cPPS,cPCD,cSDR)
  println(s4.courses)
}

/** Hints:
  * - simply implement Course, e.g. with a case class
  * - implement Student with a StudentImpl keeping a private Set of courses
  * - try to implement in StudentImpl method courses with map
  * - try to implement in StudentImpl method hasTeacher with map and find
  * - check that the two println above work correctly
  * - refactor the code so that method enrolling accepts a variable argument Course*
  */
