package u04lab.code

sealed trait Option[A] {

  def isEmpty: Boolean

  def getOrElse[B >: A](orElse: B): B

  def filter(f: A => Boolean): Option[A]

  def map[B](f: A => B): Option[B]

  def map2[B, C](opt2: Option[B])(f: (A, B) => C): Option[C]

}

case class None[A]() extends OptionImpl[A]
case class Some[A](a: A) extends OptionImpl[A]

trait OptionImpl[A] extends Option[A] {

  override def isEmpty: Boolean = this match {
    case None() => true
    case _ => false
  }

  override def getOrElse[B >: A](orElse: B): B = this match {
    case Some(x) => x
    case _ => orElse
  }

  override def filter(f: A => Boolean): Option[A] = this match {
    case Some(x) if f(x) => this
    case _ => None()
  }

  override def map[B](f: A => B): Option[B] = this match {
    case Some(x) => Some(f(x))
    case _ => None()
  }

  override def map2[B, C](opt: Option[B])(f: (A, B) => C): Option[C] =
    (this, opt) match {
      case (Some(x), Some(y)) => Some(f(x, y))
      case _ => None()
    }

}

object TestOption extends App {

  val x = Some(10)
  val y = None[Int]()

  println(x filter (_ > 2)) // Some(10)
  println(x filter (_ > 15)) // None()

  println(x map (_ > 2))    // Some(true)
  println(y map (_ > 2))    // None()

  println(x.map2(Some(3))(_ > _))      // Some(true)
  println(y.map2(Some(3))(_ > _))      // None()
  println(x.map2(None[Int]())(_ + _))  // None()
  println(x.map2(Some(3))(_ + _))      // Some(13)
  println(x.map2(Some("aaaa"))(_ + _)) // Some(10aaaa)

}