package u03lab.code

import scala.annotation.tailrec

object LabSolutions extends App{

  def compose(f: Int => Int, g: Int => Int): Int => Int = x => f(g(x))

  println(compose(_ + 1, _ * 2)(5))  // 11

  def fib(n: Int): Int = {
    @tailrec
    def fib2(n: Int, previous: Int, current: Int): Int = {
      if (n <= 0) {
        current
      } else {
        fib2(n - 1, previous = previous + current, current = previous)
      }
    }
    fib2(n, previous = 1, current = 0)
  }

  println(fib(0)) //0
  println(fib(1)) //1
  println(fib(2)) //1
  println(fib(3)) //2

  sealed trait Person
  case class Student(name: String, year: Int) extends Person
  case class Teacher(name: String, course: String) extends Person

  def name(p: Person): String = p match {
    case Student(n, _) => n
    case Teacher(n, _) => n
  }

  def personToString(p: Person): String = p match {
    case Student(name, year) => s"$name: $year"
    case Teacher(name, course) => s"$name: [$course]"
  }

  println(personToString(Teacher("mirko","PPS"))) // "mirko: [PPS]"
  println(personToString(Student("mario",2016))) // "mario: 2016"

  sealed trait Option [A]
  object Option {
    case class None [A ]() extends Option [A]
    case class Some [A ]( a: A) extends Option [A]

    def optionIsEmpty [A ](opt : Option [A ]) : Boolean = opt != None ()

    def optionGetOrElse [A , B >: A ](opt : Option [A], orElse : B): B = opt match {
      case Some ( a) => a
      case _ => orElse
    }

    def optionFilter[A](opt: Option[A])(f: A => Boolean): Option[A] = opt match {
      case Some(x) if f(x) => opt
      case _               => None()
    }

    def optionMap[A, B](opt: Option[A])(f: A => B): Option[B] = opt match {
      case Some(x) => Some(f(x))
      case _       => None()
    }

    def optionMap2[A, B, C](opt1: Option[A], opt2: Option[B])(f: (A, B) => C): Option[C] = (opt1, opt2) match {
      case (Some(x), Some(y)) => Some(f(x, y))
      case _                  => None()
    }
  }

  sealed trait List[E]

  object List {
    case class Cons[E](head: E, tail: List[E]) extends List[E]
    case class Nil[E]() extends List[E]

    def length[E](l: List[E]): Int = l match {
      case Cons(h, t) => 1 + length(t)
      case _          => 0
    }

    def sum(l: List[Int]): Int = l match {
      case Cons(h, t) => h + sum(t)
      case _          => 0
    }

    def append[A <: C, B <: C, C](l1: List[A], l2: List[B]): List[C] =
      (l1, l2) match {
        case (Cons(h, t), b) => Cons[C](h, append(t, b))
        case (a, Cons(h, t)) => Cons[C](h, append(a, t))
        case _               => Nil()
      }

    def drop[A](l: List[A], n: Int): List[A] = (l, n) match {
      case (li, 0)         => li
      case (Nil(), _)      => Nil()
      case (Cons(h, t), k) => drop(t, k - 1)
    }

    def map[A, B](l: List[A])(f: A => B): List[B] = l match {
      case Nil()      => Nil()
      case Cons(h, t) => Cons(f(h), map(t)(f))
    }

    def filter[A](l: List[A])(f: A => Boolean): List[A] = l match {
      case Nil()              => Nil()
      case Cons(h, t) if f(h) => Cons(h, filter(t)(f))
      case Cons(h, t)         => filter(t)(f)
    }

    def max(list: List[Int]): Option[Int] = {
      @tailrec
      def max2(list: List[Int], max: Option[Int]): Option[Int] = (list, max) match {
        case (Nil(), x)                            => x
        case (Cons(h, t), Option.None())           => max2(t, Option.Some(h))
        case (Cons(h, t), Option.Some(x)) if h > x => max2(t, Option.Some(h))
        case (Cons(h, t), y)                       => max2(t, y)
      }
      max2(list, Option.None())
    }

    def foldLeft[A, B](list: List[A] ,first: B) (f: (A, B) => B): B = (list, first) match {
      case (Nil(), x)      => x
      case (Cons(h, t), x) => foldLeft(t,f(h, x))(f)
    }

    def foldRight[A, B](list: List[A] ,last: B) (f: (B, A) => B): B = list match {
      case Nil()      => last
      case Cons(h, t) => f(foldRight(t , last)(f), h)
    }

  }

  import List._
  println(drop(Cons(10, Cons(20, Cons(30, Nil()))),2)) // Cons(30, Nil())
  println(drop(Cons(10, Cons(20, Cons(30, Nil()))),5)) // Nil()

  println(List.map(Cons(10, Cons(20, Nil())))(_+1)) // Cons(11, Cons(21, Nil()))
  println(List.map(Cons(10, Cons(20, Nil())))(":"+_+":")) // Cons(":10:", Cons(":20:", Nil()))

  println(filter(Cons(10, Cons(20, Nil())))(_>15)) // Cons(20, Nil())
  println(filter(Cons("a", Cons("bb", Cons("ccc", Nil()))))( _.length <=2)) // Cons("a", Cons("bb", Nil()))

  println(max(Cons(10, Cons(25, Cons(20, Nil()))))) // Some(25)
  println(max(Nil())) // None()

  val list = Cons(3,Cons(7,Cons(1,Cons(5, Nil()))))
  println(foldLeft(list,0)(_+_)) // 16
  println(foldRight(list,"")(_+_)) // "5173"

  def courses(persons: List[Person]): List[String] =
    map(filter(persons)(_.isInstanceOf[Teacher])){ case Teacher(_, c) => c }

  val teachersAndStudentsList: List[Person] = Cons(Teacher("Mirko", "PPS"),Cons(Student("Super Mario", 2018),
    Cons(Teacher("Alessandro", "PCD"), Nil())))

  println(courses(teachersAndStudentsList)) // Cons(PPS, Cons(PCD), Nil()))

  import Option._
  println(optionFilter(Some(5))(_ > 2)) // Some(5)
  println(optionFilter(Some(5))(_ > 8)) // None
  println(optionMap(Some(5))(_ > 2)) // Some(true)
  println(optionMap(None[Int])(_ > 2)) // None
}
